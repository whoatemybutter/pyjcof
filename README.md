# pyJCOF
> A Python implementation of JCOF: JSON-like Compact Object Format

[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://black.readthedocs.io/en/stable/)
[![Pylint](https://img.shields.io/badge/pylint-9.25/10.00-ffbf48)](https://pylint.pycqa.org/en/latest/)
[![License](https://img.shields.io/gitlab/license/whoatemybutter/pyjcof)](https://spdx.org/licenses/GPL-3.0-or-later.html)
[![PyPi](https://img.shields.io/pypi/v/jcof)](https://pypi.org/project/jcof/)
[![pipeline status](https://gitlab.com/whoatemybutter/pyjcof/badges/master/pipeline.svg)](https://gitlab.com/whoatemybutter/pyjcof/-/commits/master)  

For an explanation on what JCOF is, please see https://github.com/mortie/jcof.
This README does not explain the semantics of JCOF or its syntax.

## Table of contents
- [📦 Installation](#📦-installation)
- [🛠 Usage](#🛠-usage)
- [📰 Changelog](#📰-changelog)
- [📜 License](#📜-license)

---

## 📦 Installation

pyJCOF is available on PyPi. 
It requires a Python version of **at least 3.10.0.** and depends on **no packages**.

To install pyJCOF with pip:
```shell
python -m pip install jcof
```
> The package name is `jcof`, **not** `pyjcof`.

To install pyJCOF through Git:
```shell
python -m pip install git+https://gitlab.com/whoatemybutter/pyjcof.git
```

---

## 🛠 Usage

pyJCOF functions are similar to `json`.
Use `jcof.dumps` for converting Python values to JCOF,
and use `jcof.loads` for converting JCOF to Python values.

Use `dump` and `load` for dealing with files rather than objects.
```python
import jcof

example = {
    "people": [
        {"first-name": "Bob", "age": 32, "occupation": "Plumber", "full-time": True},
        {
            "first-name": "Alice",
            "age": 28,
            "occupation": "Programmer",
            "full-time": True,
        },
        {"first-name": "Bernard", "age": 36, "occupation": None, "full-time": None},
        {"first-name": "El", "age": 57, "occupation": "Programmer", "full-time": False},
    ]
}

dumped = jcof.dumps(example)
# 'Programmer;"age""first-name""full-time""occupation";{"people"[(0,iw"Bob"b"Plumber")(0,is"Alice"b,s0)(0,iA"Bernard"n,n)(0,iV"El"B,s0)]}'

assert jcof.loads(dumped) == example
# Returns nothing; True
```

---

## 📰 Changelog

The changelog is at [CHANGELOG.md](CHANGELOG.md).

---

## 📜 License

pyJCOF is licensed under
[GNU General Public License 3.0 or later](https://spdx.org/licenses/GPL-3.0-or-later.html).
<br/>
