# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2022-10-03
### Changed
- Some quick README fixes

## [1.0.0] - 2022-10-03
### Added
- Initial release

[1.0.1]: https://gitlab.com/whoatemybutter/pyjcof/-/compare/v1.0.1...HEAD
[1.0.0]: https://gitlab.com/whoatemybutter/pyjcof/-/compare/v1.0.0...v1.0.1