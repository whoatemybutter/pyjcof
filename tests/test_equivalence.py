import jcof

dumped = (
    'Programmer;"age""first-name""full-time""occupation";'
    '{"people"[(0,iw"Bob"b"Plumber")(0,is"Alice"b,s0)(0,iA"Bernard"n,n)(0,iV"El"B,s0)]}'
)


def test_dumping():
    assert jcof.dumps(jcof.EXAMPLE) == dumped
